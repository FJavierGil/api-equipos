#!/bin/bash

cd /home/wwwroot/aos
/usr/local/bin/composer update
./bin/console "doctrine:schema:update" --force
/home/wwwroot/aos/bin/console -q doctrine:query:sql "CREATE SCHEMA IF NOT EXISTS $MYSQL_TEST_DATABASE"
./vendor/bin/simple-phpunit --coverage-text

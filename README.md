![img](https://avatars1.githubusercontent.com/u/5365410?s=75) AOS - Equipos REST API
======================================

[![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)](http://opensource.org/licenses/MIT)
[![Minimum PHP Version](https://img.shields.io/badge/php-%5E7.4-blue.svg)](http://php.net/)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/FJavierGil/api-equipos/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/FJavierGil/api-equipos/?branch=master)
[![Build Status](https://scrutinizer-ci.com/b/FJavierGil/api-equipos/badges/build.png?b=master)](https://scrutinizer-ci.com/b/FJavierGil/api-equipos/build-status/master)
[![Code Coverage](https://scrutinizer-ci.com/b/FJavierGil/api-equipos/badges/coverage.png?b=master)](https://scrutinizer-ci.com/b/FJavierGil/api-equipos/?branch=master)
> Implementación de una API REST basado en el framework Symfony para la gestión de equipos.

Esta aplicación implementa una interfaz de programación [REST][rest] desarrollada como ejemplo de
utilización del framework [Symfony][symfony]. La aplicación proporciona las operaciones
habituales para la gestión de una entidad. En este proyecto se
utilizan algunos componentes del framework Symfony, el _logger_ [Monolog][monolog]
y el [ORM Doctrine][doctrine].

Adicionalmente -para hacer más sencilla la gestión de los datos- se ha utilizado
el ORM [Doctrine][doctrine]. Doctrine 2 es un Object-Relational Mapper que proporciona
persistencia transparente para objetos PHP. Utiliza el patrón [Data Mapper][dataMapper]
con el objetivo de obtener un desacoplamiento completo entre la lógica de negocio y la
persistencia de los datos en un SGBD.

Para documentar la API se incluye la especificación de empleando el esquema OpenAPI 3.0. Esta
especificación se ha elaborado empleando el editor [Swagger][swagger]. Adicionalmente se
incluye la interfaz de usuario ([Swagger-UI][swagger-ui]) de esta fenomenal herramienta que permite
realizar pruebas interactivas de manera completa y elegante.

## Despliegue con Docker

Para desplegar el proyecto empleando Docker, basta con ejecutar los dos comandos 
siguientes desde el directorio raíz del proyecto:

```
$ docker-compose up -d
$ docker exec -u dev aos_php bash -c /opt/aos/init.sh
```

La ejecución del segundo comando sólo es necesaria la primera vez que se realiza el despliegue.

Una vez deplegado el proyecto, se puede acceder a la interfaz de usuario de la especificación 
a través de [http://localhost/][lh]. Además, también se podrá acceder a la herramienta
phpMyAdmin en [http://localhost:8080/][pma].


Para detener la ejecución de la aplicación se deberá ejecutar el comando:

```
$ docker-compose stop
```

## Instalación de la aplicación _on-premise_

El primer paso consiste en generar un esquema de base de datos vacío y un usuario/contraseña
con privilegios completos sobre dicho esquema.

A continuación se deberá crear una copia del fichero `./.env` y renombrarla
como `./.env.local`. Después se debe editar dicho fichero y modificar la variable `DATABASE_URL`
con los siguientes parámetros:

* Nombre y contraseña del usuario generado anteriormente
* Nombre del esquema de bases de datos

Una vez editado el anterior fichero y desde el directorio raíz del proyecto se deben ejecutar los comandos:
```
$ composer install
$ bin\console doctrine:schema:update --dump-sql --force
```
## Estructura del proyecto:

El contenido y estructura del proyecto es:

* Directorio raíz del proyecto `.`:
    - `.env`: valores por defecto para las variables necesarias para la aplicación 
    - `.env.local`: variables de entorno locales (sobreescribe valores de `.env`)
    - `phpunit.xml.dist` configuración de la suite de pruebas
    - `README.md`: este fichero
* Directorio `bin`:
    - Ejecutables (*console* y *phpunit*)
* Directorio `src`:
    - Contiene el código fuente de la aplicación
    - Subdirectorio `src/Entity`: entidades PHP (incluyen anotaciones de mapeo del ORM)
* Directorio `var`:
    - Subdirectorios de `log` y `cache` (diferenciando por entornos).
* Directorio `public`:
    - `index.php` es el controlador frontal de la aplicación. Inicializa y lanza 
      el núcleo de la aplicación.
    - Subdirectorio `api-docs`: cliente [Swagger][swagger] y especificación de la API Rest.
* Directorio `vendor`:
    - Componentes desarrollados por terceros (Symfony, Doctrine, Monolog, Dotenv, etc.)
* Directorio `tests`:
    - Conjunto de scripts para la ejecución de pruebas con PHPUnit.

## Ejecución de pruebas

La aplicación incorpora un conjunto de herramientas para la ejecución de pruebas 
unitarias y de integración con [PHPUnit][phpunit]. Empleando este conjunto de herramientas
es posible comprobar de manera automática el correcto funcionamiento de la API completa
sin la necesidad de herramientas adicionales.

Para configurar el entorno de pruebas se debe crear un nuevo esquema de bases de datos vacío,
y una copia del fichero `./phpunit.xml.dist` y renombrarla como `./phpunit.xml`. De igual
forma se deberá crear una copia del fichero `./.env.test` y renombrarla como
`./.env.test.local`. Después se debe editar este último fichero para asignar los
siguientes parámetros:
                                                                            
* Configuración del acceso a la nueva base de datos (variable `DATABASE_URL`)
* Nombre y contraseña de los usuarios que se van a emplear para realizar las pruebas (no
es necesario insertarlos, lo hace automáticamente el método `setUpBeforeClass()`
de la clase `BaseTestCase`)

Para lanzar la suite de pruebas completa se debe ejecutar:
```
$ ./vendor/bin/simple-phpunit [--testdox] [--coverage-text]
```

[dataMapper]: http://martinfowler.com/eaaCatalog/dataMapper.html
[doctrine]: http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/
[lh]: https://localhost:80/
[pma]: https://localhost:8080/
[monolog]: https://github.com/Seldaek/monolog
[openapi]: https://www.openapis.org/
[phpunit]: https://phpunit.readthedocs.io/en/7.5/
[rest]: http://www.restapitutorial.com/
[symfony]: https://symfony.com/
[swagger]: http://swagger.io/
[swagger-ui]: https://github.com/swagger-api/swagger-ui
[lexik]: https://github.com/lexik/LexikJWTAuthenticationBundle
[1]: https://github.com/lexik/LexikJWTAuthenticationBundle/blob/master/Resources/doc/index.md#generate-the-ssh-keys
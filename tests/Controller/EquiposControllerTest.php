<?php

namespace App\Tests\Controller;

use App\Controller\EquiposController;
use App\Entity\Message;
use App\Entity\Equipo;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class EquiposControllerTest
 *
 * @package App\Tests\Controller
 *
 * @coversDefaultClass \App\Controller\EquiposController
 */
class EquiposControllerTest extends BaseTestCase
{
    private const RUTA_API = EquiposController::RUTA_API;

    /**
     * Test OPTIONS /equipos[/idEquipo] 200 Ok
     *
     * @covers ::__construct
     * @covers ::optionsAction
     * @return void
     */
    public function testOptionsEquipoAction200(): void
    {
        self::$client->request(
            Request::METHOD_OPTIONS,
            self::RUTA_API
        );
        $response = self::$client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode());
        self::assertNotEmpty($response->headers->get('Allow'));

        self::$client->request(
            Request::METHOD_OPTIONS,
            self::RUTA_API . '/' . self::$faker->numberBetween(1, 100)
        );

        self::assertSame(Response::HTTP_OK, $response->getStatusCode());
        self::assertNotEmpty($response->headers->get('Allow'));
    }

    /**
     * Test GET /equipos 404 Not Found
     *
     * @return void
     */
    public function testCgetAction404(): void
    {
        self::$client->request(
            Request::METHOD_GET,
            self::RUTA_API
        );
        $response = self::$client->getResponse();

        self::assertSame(
            Response::HTTP_NOT_FOUND,
            $response->getStatusCode()
        );
        $r_body = (string) $response->getContent();
        self::assertContains(Message::CODE_ATTR, $r_body);
        self::assertContains(Message::MESSAGE_ATTR, $r_body);
        $r_data = json_decode($r_body, true, 512, JSON_THROW_ON_ERROR);
        self::assertSame(
            Response::HTTP_NOT_FOUND,
            $r_data[Message::CODE_ATTR]
        );
        self::assertSame(
            Response::$statusTexts[404],
            $r_data[Message::MESSAGE_ATTR]
        );
    }

    /**
     * Test POST /equipos 201 Created
     *
     * @return array equipo data
     */
    public function testPostEquipoAction201(): array
    {
        $p_data = [
            Equipo::NOMBRE_ATTR => self::$faker->words(2, true),
            Equipo::FUNDACION_ATTR => (int) self::$faker->year,
            Equipo::URL_ATTR => self::$faker->url,
        ];

        // 201
        self::$client->request(
            Request::METHOD_POST,
            self::RUTA_API,
            [],
            [],
            [],
            json_encode($p_data, JSON_THROW_ON_ERROR)
        );
        $response = self::$client->getResponse();

        self::assertSame(Response::HTTP_CREATED, $response->getStatusCode());
        self::assertTrue($response->isSuccessful());
        self::assertNotNull($response->headers->get('Location'));
        self::assertJson($response->getContent());
        $equipo = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        self::assertNotEmpty($equipo[Equipo::ROOT_ATTR]['id']);
        self::assertSame(   // nombre
            $p_data[Equipo::NOMBRE_ATTR],
            $equipo[Equipo::ROOT_ATTR][Equipo::NOMBRE_ATTR]
        );
        self::assertSame(   // año fundación
            $p_data[Equipo::FUNDACION_ATTR],
            $equipo[Equipo::ROOT_ATTR][Equipo::FUNDACION_ATTR]
        );
        self::assertSame(   // URL
            $p_data[Equipo::URL_ATTR],
            $equipo[Equipo::ROOT_ATTR][Equipo::URL_ATTR]
        );

        return $equipo[Equipo::ROOT_ATTR];
    }

    /**
     * Test GET /equipos 200 Ok
     *
     * @return void
     * @depends testPostEquipoAction201
     */
    public function testCgetAction200(): void
    {
        self::$client->request(
            Request::METHOD_GET,
            self::RUTA_API
        );
        $response = self::$client->getResponse();

        self::assertTrue($response->isSuccessful());
        self::assertTrue($response->headers->has('etag'));
        $responseContent = $response->getContent();
        self::assertJson($responseContent);
        $equipos = json_decode($responseContent, true, 512, JSON_THROW_ON_ERROR);
        self::assertArrayHasKey('equipos', $equipos);
    }

    /**
     * Test GET /equipos 200 Ok (XML)
     *
     * @param   array $equipo equipo returned by testPostEquipoAction201()
     * @return  void
     * @depends testPostEquipoAction201
     */
    public function testCgetAction200Xml(array $equipo): void
    {
        self::$client->request(
            Request::METHOD_GET,
            self::RUTA_API . '/' . $equipo['id'] . '.xml'
        );
        $response = self::$client->getResponse();

        self::assertTrue($response->isSuccessful());
        self::assertTrue($response->headers->has('etag'));
        self::assertSame(
            'application/xml',
            $response->headers->get('content-type')
        );
    }

    /**
     * Test GET /equipos/{idEquipo} 200 Ok
     *
     * @param   array $equipo equipo returned by testPostEquipoAction201()
     * @return  void
     * @depends testPostEquipoAction201
     */
    public function testGetEquipoAction200(array $equipo): void
    {
        self::$client->request(
            Request::METHOD_GET,
            self::RUTA_API . '/' . $equipo['id']
        );
        $response = self::$client->getResponse();

        self::assertNotNull($response->getEtag());
        self::assertSame(Response::HTTP_OK, $response->getStatusCode());
        self::assertJson((string) $response->getContent());
        $equipo_aux = json_decode((string) $response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        self::assertSame(   // id
            $equipo['id'],
            $equipo_aux[Equipo::ROOT_ATTR]['id']
        );
        self::assertSame(   // nombre
            $equipo[Equipo::NOMBRE_ATTR],
            $equipo_aux[Equipo::ROOT_ATTR][Equipo::NOMBRE_ATTR]
        );
        self::assertSame(   // año fundación
            $equipo[Equipo::FUNDACION_ATTR],
            $equipo_aux[Equipo::ROOT_ATTR][Equipo::FUNDACION_ATTR]
        );
        self::assertSame(   // URL
            $equipo[Equipo::URL_ATTR],
            $equipo_aux[Equipo::ROOT_ATTR][Equipo::URL_ATTR]
        );
    }

    /**
     * Test POST /equipos 400 Bad Request
     *
     * @param   array $equipo equipo returned by testPostEquipoAction201()
     * @return  void
     * @depends testPostEquipoAction201
     */
    public function testPostEquipoAction400(array $equipo): void
    {
        $p_data = [
            Equipo::NOMBRE_ATTR => $equipo[Equipo::NOMBRE_ATTR], // mismo nombre
        ];
        self::$client->request(
            Request::METHOD_POST,
            self::RUTA_API,
            [],
            [],
            [],
            json_encode($p_data, JSON_THROW_ON_ERROR)
        );
        $response = self::$client->getResponse();

        self::assertSame(
            Response::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
        $r_body = (string) $response->getContent();
        self::assertJson($r_body);
        self::assertContains(Message::CODE_ATTR, $r_body);
        self::assertContains(Message::MESSAGE_ATTR, $r_body);
        $r_data = json_decode($r_body, true, 512, JSON_THROW_ON_ERROR);
        self::assertSame(
            Response::HTTP_BAD_REQUEST,
            $r_data[Message::CODE_ATTR]
        );
        self::assertSame(
            Response::$statusTexts[400],
            $r_data[Message::MESSAGE_ATTR]
        );
    }

    /**
     * Test PUT /equipo/{idEquipo} 209 Content Returned
     *
     * @param   array $equipo equipo returned by testPostEquipoAction201()
     * @return  array modified equipo data
     * @depends testPostEquipoAction201
     */
    public function testPutEquipoAction209(array $equipo): array
    {
        $p_data = [
            Equipo::NOMBRE_ATTR => self::$faker->name,
            Equipo::FUNDACION_ATTR => (int) self::$faker->numberBetween(0, 2500),
            Equipo::URL_ATTR => self::$faker->url,
        ];

        self::$client->request(
            Request::METHOD_PUT,
            self::RUTA_API . '/' . $equipo['id'],
            [],
            [],
            [],
            json_encode($p_data, JSON_THROW_ON_ERROR)
        );
        $response = self::$client->getResponse();

        self::assertSame(209, $response->getStatusCode());
        self::assertJson((string) $response->getContent());
        $equipo_aux = json_decode((string) $response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        self::assertSame($equipo['id'], $equipo_aux[Equipo::ROOT_ATTR]['id']);
        self::assertSame(   // nombre
            $p_data[Equipo::NOMBRE_ATTR],
            $equipo_aux[Equipo::ROOT_ATTR][Equipo::NOMBRE_ATTR]
        );
        self::assertSame(   // año fundación
            $p_data[Equipo::FUNDACION_ATTR],
            $equipo_aux[Equipo::ROOT_ATTR][Equipo::FUNDACION_ATTR]
        );
        self::assertSame(   // URL
            $p_data[Equipo::URL_ATTR],
            $equipo_aux[Equipo::ROOT_ATTR][Equipo::URL_ATTR]
        );

        return $equipo_aux[Equipo::ROOT_ATTR];
    }

    /**
     * Test PUT /equipo/{idEquipo} 400 Bad Request
     *
     * @param   array $equipo equipo returned by testPutEquipoAction209()
     * @return  void
     * @depends testPutEquipoAction209
     */
    public function testPutEquipoAction400(array $equipo): void
    {
        // nombre already exists
        $p_data = [
            Equipo::NOMBRE_ATTR => $equipo[Equipo::NOMBRE_ATTR]
        ];
        self::$client->request(
            Request::METHOD_PUT,
            self::RUTA_API . '/' . $equipo['id'],
            [],
            [],
            [],
            json_encode($p_data, JSON_THROW_ON_ERROR)
        );
        $response = self::$client->getResponse();

        self::assertSame(
            Response::HTTP_BAD_REQUEST,
            $response->getStatusCode()
        );
        $r_body = (string) $response->getContent();
        self::assertJson($r_body);
        self::assertContains(Message::CODE_ATTR, $r_body);
        self::assertContains(Message::MESSAGE_ATTR, $r_body);
        $r_data = json_decode($r_body, true, 512, JSON_THROW_ON_ERROR);
        self::assertSame(
            Response::HTTP_BAD_REQUEST,
            $r_data[Message::CODE_ATTR]
        );
        self::assertSame(
            Response::$statusTexts[400],
            $r_data[Message::MESSAGE_ATTR]
        );
    }

    /**
     * Test DELETE /equipo/{idEquipo} 204 No Content
     *
     * @param   array $equipo equipo returned by testPostEquipoAction201()
     * @return  int idEquipo
     * @depends testPostEquipoAction201
     * @depends testPostEquipoAction400
     * @depends testGetEquipoAction200
     * @depends testPutEquipoAction400
     */
    public function testDeleteEquipoAction204(array $equipo): int
    {
        self::$client->request(
            Request::METHOD_DELETE,
            self::RUTA_API . '/' . $equipo['id']
        );
        $response = self::$client->getResponse();

        self::assertSame(
            Response::HTTP_NO_CONTENT,
            $response->getStatusCode()
        );
        self::assertEmpty((string) $response->getContent());

        return $equipo['id'];
    }

    /**
     * Test POST /equipos 422 Unprocessable Entity
     *
     * @param null|string $nombre
     * @dataProvider equipoProvider422
     * @return void
     */
    public function testPostEquipoAction422(?string $nombre): void
    {
        $p_data = [
            Equipo::NOMBRE_ATTR => $nombre,
        ];

        self::$client->request(
            Request::METHOD_POST,
            self::RUTA_API,
            [],
            [],
            [],
            json_encode($p_data, JSON_THROW_ON_ERROR)
        );
        $response = self::$client->getResponse();

        self::assertEquals(
            Response::HTTP_UNPROCESSABLE_ENTITY,
            $response->getStatusCode()
        );
        $r_body = (string) $response->getContent();
        self::assertJson($r_body);
        self::assertContains(Message::CODE_ATTR, $r_body);
        self::assertContains(Message::MESSAGE_ATTR, $r_body);
        $r_data = json_decode($r_body, true, 512, JSON_THROW_ON_ERROR);
        self::assertSame(
            Response::HTTP_UNPROCESSABLE_ENTITY,
            $r_data[Message::CODE_ATTR]
        );
        self::assertSame(
            Response::$statusTexts[422],
            $r_data[Message::MESSAGE_ATTR]
        );
    }


    /**
     * Test GET    /equipo/{idEquipo} 404 NOT FOUND
     * Test PUT    /equipo/{idEquipo} 404 NOT FOUND
     * Test DELETE /equipo/{idEquipo} 404 NOT FOUND
     *
     * @param string $method
     * @param int $idEquipo equipo id. returned by testDeleteEquipoAction204()
     * @dataProvider routeProvider404
     * @return void
     * @depends testDeleteEquipoAction204
     */
    public function testEquipoStatus404(string $method, int $idEquipo): void
    {
        self::$client->request(
            $method,
            self::RUTA_API . '/' . $idEquipo
        );
        $response = self::$client->getResponse();

        self::assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
        $r_body = (string) $response->getContent();
        self::assertContains(Message::CODE_ATTR, $r_body);
        self::assertContains(Message::MESSAGE_ATTR, $r_body);
        $r_data = json_decode($r_body, true, 512, JSON_THROW_ON_ERROR);
        self::assertSame(Response::HTTP_NOT_FOUND, $r_data[Message::CODE_ATTR]);
        self::assertSame(Response::$statusTexts[404], $r_data[Message::MESSAGE_ATTR]);
    }

    /**
     * *********
     * PROVIDERS
     * *********
     */

    /**
     * Equipo provider (incomplete) -> 422 status code
     *
     * @return array equipo data
     */
    public function equipoProvider422(): array
    {
        return [
            'nulo_01' => [ null,   null ],
        ];
    }

    /**
     * Route provider (expected status 404 NOT FOUND)
     *
     * @return array [ method ]
     */
    public function routeProvider404(): array
    {
        return [
            'getAction404'    => [ Request::METHOD_GET ],
            'putAction404'    => [ Request::METHOD_PUT ],
            'deleteAction404' => [ Request::METHOD_DELETE ],
        ];
    }
}

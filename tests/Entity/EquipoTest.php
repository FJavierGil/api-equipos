<?php

/**
 * PHP version 7.4
 *
 * @category TestEntities
 * @package  App\Tests\Entity
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     http://www.etsisi.upm.es/ E.T.S. de Ingeniería de Sistemas Informáticos
 */

namespace App\Tests\Entity;

use App\Entity\Equipo;
use Exception;
use Faker\Factory as FakerFactoryAlias;
use Faker\Generator as FakerGeneratorAlias;
use PHPUnit\Framework\TestCase;

/**
 * Class EquipoTest
 *
 * @package App\Tests\Entity
 *
 * @coversDefaultClass \App\Entity\Equipo
 */
class EquipoTest extends TestCase
{

    protected static Equipo $equipo;

    private static FakerGeneratorAlias $faker;

    /**
     * Sets up the fixture.
     * This method is called before a test is executed.
     */
    public static function setUpBeforeClass(): void
    {
        self::$equipo = new Equipo('');
        self::$faker = FakerFactoryAlias::create('es_ES');
    }

    /**
     * Implement testConstructor().
     *
     * @return void
     */
    public function testConstructor(): void
    {
        $nombre = self::$faker->words(2, true);
        self::$equipo = new Equipo($nombre);
        self::assertNull(self::$equipo->getId());
        self::assertEquals($nombre, self::$equipo->getNombre());
        self::assertNull(self::$equipo->getFundacion());
        self::assertNull(self::$equipo->getUrl());
    }

    /**
     * Implement testGetId().
     *
     * @return void
     */
    public function testGetId(): void
    {
        self::assertEmpty(self::$equipo->getId());
    }

    /**
     * Implements testGetSetNombre().
     *
     * @throws Exception
     * @return void
     */
    public function testGetSetNombre(): void
    {
        $nombre = self::$faker->words(2, true);
        self::$equipo->setNombre($nombre);
        static::assertSame(
            $nombre,
            self::$equipo->getNombre()
        );
    }

    /**
     * Implements testGetSetFundacion().
     *
     * @throws Exception
     * @return void
     */
    public function testGetSetFundacion(): void
    {
        $year = self::$faker->randomDigitNotNull;
        self::$equipo->setFundacion($year);
        static::assertSame(
            $year,
            self::$equipo->getFundacion()
        );
    }

    /**
     * Implements testGetSetUrl().
     *
     * @throws Exception
     * @return void
     */
    public function testGetSetUrl(): void
    {
        $url = self::$faker->url;
        self::$equipo->setUrl($url);
        static::assertSame(
            $url,
            self::$equipo->getUrl()
        );
    }
}

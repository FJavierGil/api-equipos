<?php

namespace App\Controller;

use App\Entity\Equipo;
use App\Entity\Message;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class EquiposController
 *
 * @package App\Controller
 *
 * @Route(
 *     path=EquiposController::RUTA_API,
 *     name="api_equipo_"
 * )
 */
class EquiposController extends AbstractController
{
    use Utils;

    public const RUTA_API = '/api/v1/equipos';

    private const HEADER_CACHE_CONTROL = 'Cache-Control';
    private const HEADER_ETAG = 'ETag';
    private const HEADER_ALLOW = 'Allow';

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    /**
     * CGET Action
     *
     * @param   Request $request
     * @return  Response
     * @Route(
     *     path=".{_format}",
     *     defaults={ "_format": null },
     *     requirements={
     *         "_format": "json|xml"
     *     },
     *     methods={ Request::METHOD_GET },
     *     name="cget"
     * )
     */
    public function cgetAction(Request $request): Response
    {
        /** @var Equipo[] $equipos */
        $equipos = $this->entityManager
            ->getRepository(Equipo::class)
            ->findAll();
        $format = Utils::getFormat($request);

        // Vacío?
        if (empty($equipos)) {
            return $this->error404($format);
        }
        $equipos = [
            'equipos' => array_map(fn($e) => [Equipo::ROOT_ATTR => $e], $equipos)
        ];

        return Utils::apiResponse(
            Response::HTTP_OK,
            $equipos,
            $format,
            [
                self::HEADER_CACHE_CONTROL => 'must-revalidate',
                self::HEADER_ETAG => md5(json_encode($equipos, JSON_THROW_ON_ERROR)),
            ]
        );
    }

    /**
     * GET Action
     *
     * @param Request $request
     * @param  int $idEquipo Equipo id
     * @return Response
     * @Route(
     *     path="/{idEquipo}.{_format}",
     *     defaults={ "_format": null },
     *     requirements={
     *          "idEquipo": "\d+",
     *          "_format": "json|xml"
     *     },
     *     methods={ Request::METHOD_GET },
     *     name="get"
     * )
     */
    public function getAction(Request $request, int $idEquipo): Response
    {
        $equipo = $this->entityManager
            ->getRepository(Equipo::class)
            ->find($idEquipo);
        $format = Utils::getFormat($request);

        if (empty($equipo)) {
            return $this->error404($format);
        }

        return Utils::apiResponse(
            Response::HTTP_OK,
            [ Equipo::ROOT_ATTR => $equipo ],
            $format,
            [
                self::HEADER_CACHE_CONTROL => 'must-revalidate',
                self::HEADER_ETAG => md5(json_encode($equipo, JSON_THROW_ON_ERROR)),
            ]
        );
    }

    /**
     * Summary: Provides the list of HTTP supported methods
     * Notes: Return a &#x60;Allow&#x60; header with a list of HTTP supported methods.
     *
     * @param int $idEquipo
     * @return Response
     * @Route(
     *     path="/{idEquipo}.{_format}",
     *     defaults={ "idEquipo" = 0, "_format": null },
     *     requirements={
     *          "idEquipo": "\d+",
     *         "_format": "json|xml"
     *     },
     *     methods={ Request::METHOD_OPTIONS },
     *     name="options"
     * )
     */
    public function optionsAction(int $idEquipo): Response
    {
        $methods = $idEquipo
            ? [ Request::METHOD_GET, Request::METHOD_PUT, Request::METHOD_DELETE ]
            : [ Request::METHOD_GET, Request::METHOD_POST ];

        return Utils::apiResponse(
            Response::HTTP_OK,
            null,
            null,
            [
                self::HEADER_ALLOW => implode(', ', $methods),
                self::HEADER_CACHE_CONTROL => 'public, inmutable'
            ]

        );
    }

    /**
     * DELETE Action
     *
     * @param   Request $request
     * @param   int $idEquipo User id
     * @return  Response
     * @Route(
     *     path="/{idEquipo}.{_format}",
     *     defaults={ "_format": null },
     *     requirements={
     *          "idEquipo": "\d+",
     *         "_format": "json|xml"
     *     },
     *     methods={ Request::METHOD_DELETE },
     *     name="delete"
     * )
     */
    public function deleteAction(Request $request, int $idEquipo): Response
    {
        $format = Utils::getFormat($request);

        /** @var Equipo $equipo */
        $equipo = $this->entityManager
            ->getRepository(Equipo::class)
            ->find($idEquipo);

        if (null === $equipo) {   // 404 - Not Found
            return $this->error404($format);
        }

        $this->entityManager->remove($equipo);
        $this->entityManager->flush();

        return Utils::apiResponse(Response::HTTP_NO_CONTENT);
    }

    /**
     * POST action
     *
     * @param Request $request request
     * @return Response
     * @Route(
     *     path=".{_format}",
     *     defaults={ "_format": null },
     *     requirements={
     *         "_format": "json|xml"
     *     },
     *     methods={ Request::METHOD_POST },
     *     name="post"
     * )
     */
    public function postAction(Request $request): Response
    {
        $body = $request->getContent();
        $postData = json_decode($body, true, 512, JSON_THROW_ON_ERROR);
        $format = Utils::getFormat($request);

        if (!isset($postData[Equipo::NOMBRE_ATTR])) {
            // 422 - Unprocessable Entity Faltan datos
            $message = new Message(Response::HTTP_UNPROCESSABLE_ENTITY, Response::$statusTexts[422]);
            return Utils::apiResponse(
                $message->getCode(),
                $message,
                $format
            );
        }

        // hay datos -> procesarlos
        $element_exist = $this->entityManager
                ->getRepository(Equipo::class)
                ->findOneBy([ Equipo::NOMBRE_ATTR => $postData[Equipo::NOMBRE_ATTR] ]);

        if (null !== $element_exist) {    // 400 - Bad Request
            $message = new Message(Response::HTTP_BAD_REQUEST, Response::$statusTexts[400]);
            return Utils::apiResponse(
                $message->getCode(),
                $message,
                $format
            );
        }

        // 201 - Created
        $equipo = new Equipo(
            $postData[Equipo::NOMBRE_ATTR]
        );
        // fundacion
        if (isset($postData[Equipo::FUNDACION_ATTR])) {
            $equipo->setFundacion($postData[Equipo::FUNDACION_ATTR]);
        }
        // url
        if (isset($postData[Equipo::URL_ATTR])) {
            $equipo->setUrl($postData[Equipo::URL_ATTR]);
        }

        $this->entityManager->persist($equipo);
        $this->entityManager->flush();

        return Utils::apiResponse(
            Response::HTTP_CREATED,
            [ Equipo::ROOT_ATTR => $equipo ],
            $format,
            [
                'Location' => self::RUTA_API . '/' . $equipo->getId(),
            ]
        );
    }

    /**
     * PUT action
     *
     * @param   Request $request request
     * @param   int $idEquipo User id
     * @return  Response
     * @Route(
     *     path="/{idEquipo}.{_format}",
     *     defaults={ "_format": null },
     *     requirements={
     *          "idEquipo": "\d+",
     *         "_format": "json|xml"
     *     },
     *     methods={ Request::METHOD_PUT },
     *     name="put"
     * )
     */
    public function putAction(Request $request, int $idEquipo): Response
    {
        $body = $request->getContent();
        $postData = json_decode($body, true);
        $format = Utils::getFormat($request);

        $equipo = $this->entityManager
            ->getRepository(Equipo::class)
            ->find($idEquipo);

        if (null === $equipo) {    // 404 - Not Found
            return $this->error404($format);
        }

        if (isset($postData[Equipo::NOMBRE_ATTR])) {
            $user_exist = $this->entityManager
                ->getRepository(Equipo::class)
                ->findOneBy([ Equipo::NOMBRE_ATTR => $postData[Equipo::NOMBRE_ATTR] ]);

            if (null !== $user_exist) {    // 400 - Bad Request
                $message = new Message(Response::HTTP_BAD_REQUEST, Response::$statusTexts[400]);
                return Utils::apiResponse(
                    $message->getCode(),
                    $message,
                    $format
                );
            }
            $equipo->setNombre($postData[Equipo::NOMBRE_ATTR]);
        }

        // fundacion
        if (isset($postData[Equipo::FUNDACION_ATTR])) {
            $equipo->setFundacion((int) $postData[Equipo::FUNDACION_ATTR]);
        }

        // url
        if (isset($postData[Equipo::URL_ATTR])) {
            $equipo->setUrl($postData[Equipo::URL_ATTR]);
        }

        $this->entityManager->flush();

        return Utils::apiResponse(
            209,                        // 209 - Content Returned
            [ Equipo::ROOT_ATTR => $equipo ],
            $format
        );
    }

    /**
     * Response 404 Not Found
     * @param string $format
     *
     * @return Response
     */
    private function error404(string $format): Response
    {
        $message = new Message(Response::HTTP_NOT_FOUND, Response::$statusTexts[404]);
        return Utils::apiResponse(
            $message->getCode(),
            $message,
            $format
        );
    }
}

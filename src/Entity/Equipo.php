<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity()
 *
 * @Hateoas\Relation(
 *     name = "parent",
 *     href = "expr(constant('\\App\\Controller\\EquiposController::RUTA_API'))"
 * )
 *
 * @Hateoas\Relation(
 *     name = "self",
 *     href = "expr(constant('\\App\\Controller\\EquiposController::RUTA_API') ~ '/' ~ object.getId())"
 * )
 */
class Equipo
{
    public const ROOT_ATTR = 'equipo';
    public const NOMBRE_ATTR = 'nombre';
    public const FUNDACION_ATTR = 'fundacion';
    public const URL_ATTR = 'url';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private string $nombre;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $fundacion = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $url = null;

    /**
     * Equipo constructor.
     * @param string $nombre
     * @param int|null $fundacion
     * @param string|null $url
     */
    public function __construct(string $nombre, ?int $fundacion = null, ?string $url = null)
    {
        $this->nombre = $nombre;
        $this->fundacion = $fundacion;
        $this->url = $url;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getFundacion(): ?int
    {
        return $this->fundacion;
    }

    /**
     * @param int|null $fundacion
     * @return Equipo
     */
    public function setFundacion(?int $fundacion): Equipo
    {
        $this->fundacion = $fundacion;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     * @return Equipo
     */
    public function setUrl(?string $url): Equipo
    {
        $this->url = $url;
        return $this;
    }
}
